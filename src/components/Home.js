import React from "react";
import { getImages, postImage } from "../clientApi";

class Home extends React.Component {
  state = {
    getImages: { imageURIs: [], statusCode: 0 },
    postImage: { imageURIs: "", statusCode: 0 }
  };

  componentDidMount() {
    getImages().then(result => {
      this.setState({ ...this.state, getImages: result });
    });
  }

  handleImageUpload = event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    // would send to action creator call
    postImage(formData).then(result => {
      this.setState({ ...this.state, postImage: result });
    });
  };

  render() {
    return (
      <>
        <h1>Kenziegram</h1>
        <form onSubmit={this.handleImageUpload}>
          <input type="file" />
          <button type="submit"> Upload Image</button>
        </form>
        {this.state.postImage.statusCode === 200 && "Upload was successful!"}
        {this.state.getImages.imageURIs.map(uri => (
          <img key={uri} src={uri} />
        ))}
      </>
    );
  }
}

export default Home;
