export const getImagesResponse = {
  imageURIs: [
    "http://r.ddmcdn.com/s_f/o_1/cx_462/cy_245/cw_1349/ch_1349/w_720/APL/uploads/2015/06/caturday-shutterstock_149320799.jpg",
    "https://media.newyorker.com/photos/5ab95b9e8d2914101d1816ee/master/pass/Obsessions-How-Cats-Tamed-Us.jpg"
  ],
  statusCode: 200
};

export const postImageResponse = {
  imageURIs:
    "https://i1.wp.com/metro.co.uk/wp-content/uploads/2018/06/sei_17772128.jpg?quality=90&strip=all&zoom=1&resize=964%2C642&ssl=1",
  statusCode: 200
};
